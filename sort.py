import pathlib
import shutil

try:
    from h5rdmtoolbox.conventions.cflike import StandardNameTable
except ImportError:
    raise ImportError('h5rdmtoolbox missing. you need to install it to check for standard name tables')
__this_dir__ = pathlib.Path(__file__).parent


def sort():
    """Sorts the standard name table"""

    shutil.copy(__this_dir__ / 'open_centrifugal_fan_database-v1.yaml',
                __this_dir__ / 'open_centrifugal_fan_database-v1.yaml.bak')
    snt_local = StandardNameTable.from_yaml(
        __this_dir__ / 'open_centrifugal_fan_database-v1.yaml.bak'
    )
    snt_local.to_yaml(__this_dir__ / 'open_centrifugal_fan_database-v1.yaml')  # automatically sorts


if __name__ == '__main__':
    sort()
